<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AnimalController;
use App\Http\Controllers\InicioController;
use App\Http\Controllers\CuidadorController;
use App\Http\Controllers\TitulacionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [InicioController::class,'inicio'])->name('home');

Route::get('animales',[AnimalController::class,'index'])->name('animales.index');

Route::get('animales/crear',[AnimalController::class,'create'])
   
    ->name('animales.create');

Route::get('cuidadores/{cuidador}',[CuidadorController::class,'show'])->name('cuidadores.show');

Route::get('titulaciones/{titulacion}',[TitulacionController::class,'show'])->name('titulaciones.show');

Route::get('animales/{animal}/createRevision',[AnimalController::class,'crearRevision'])
    ->middleware('auth')
    ->name('animales.createRevision');

Route::post('animales/',[AnimalController::class,'addRevision'])    
    ->name('animales.addRevision');



Route::get('animales/{animal}',[Animalcontroller::class,'show'])->name('animales.show');


Route::post('animales/crear',[AnimalController::class,'store'])->name('animales.store');

Route::get('animales/{animal}/editar',[AnimalController::class,'edit'])
    
    ->name('animales.edit');

Route::put('animales',[AnimalController::class,'update'])->name('animales.update');
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
