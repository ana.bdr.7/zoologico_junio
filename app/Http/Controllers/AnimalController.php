<?php

namespace App\Http\Controllers;
use App\Http\Controllers\InicioController;
use Illuminate\Support\Facades\DB;
use App\Models\Animal;
use App\Models\Revision;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class AnimalController extends Controller
{
    public function index(){
       
        $animales = Animal::with('revisiones','cuidadores')->get();
        

        return view('animales.index',['animales'=>$animales]);
    }

    public function show(Animal $animal){
        
        return view('animales.show',['animal'=> $animal]);
    }

    public function create(){
        
        return view('animales.create');
    }

    public function crearRevision(Animal $animal){
        
        $animal = Animal::find($animal);

        return view('animales.createRevision',['animal'=>$animal]);
    }

    public function addRevision(Request $request){
        $revision = $request->except('_token');
        $revision['id_animal'] = $request->id_animal;
        $revision['fechaRevision'] = $request->fechaRevision;
        $revision['descripcion'] = $request['descripcion'];

        try{
            $revision = Revision::create($revision);
            return redirect()->route('animales.index')->with('mensaje','Revisión subido correctamente');
        }catch(Illuminate\Database\QueryException $ex){
            return redirect()->route('animales.createRevision')->with('mensaje','Ha ocurrido un error');
        }
    }

    public function store(Request $request){
        $datos = $request->except('_token');
        $datos['slug'] = Str::slug($request->especie);
        $datos['imagen'] = $request->imagen->store('','animals');

        try{
            $animal = Animal::create($datos);
            return redirect()->route('animales.create')->with('mensaje','Animal subido correctamente');
        }catch(Illuminate\Database\QueryException $ex){
            return redirect()->route('animales.create')->with('mensaje','Ha ocurrido un error');
        }
    }

    public function edit($animal_id){
        $animal = Animal::find($animal_id);

        return view('animales.edit',['animal' => $animal]);
    }

    public function update(Request $request){
        
        $animal = Animal::find($request->id);
        
        $animal->especie = $request->especie;
        $animal->slug = Str::slug($request->especie);
        $animal->peso = $request->peso;
        $animal->altura = $request->altura;
        $animal->fechaNacimiento = $request->fechaNacimiento;
        $animal->alimentacion = $request->alimentacion;
        $animal->descripcion = $request->descripcion;
 
        if(!empty($request->imagen)){
            Storage::disk('animals')->delete($animal->imagen);
            $animal->imagen = $request->imagen->store('','animals');
        }
        try{
            $animal->save();
            $animal->cuidadores()->sync($request->id_cuidador);

            return redirect()->route('animales.show',$animal->slug)->with('mensaje','Animal modificado correctamente');
        }catch(Illuminate\Database\QueryException $ex){
            return redirect()->route('animales.show',$animal->slug)->with('mensaje','Fallo al modificar el animal');
        }

        
    }

    
}