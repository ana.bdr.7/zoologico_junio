<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Animal extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function revisiones(){
        return $this->hasMany(Revision::class,'id_animal');
    }

    public function cuidadores(){
        return $this->belongsToMany(Cuidador::class, 'animal_cuidador','id_animal','id_cuidador');
    }
    public function getRouteKeyName(){
        return 'slug';
    }
    public function getEdad(){
        $fechaFormateada = Carbon::parse($this->fechaNacimiento);
        return $fechaFormateada->diffInYears(Carbon::now());
    }
    
}
