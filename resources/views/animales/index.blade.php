@extends('layouts.master')

@section('titulo')
    zoologico
@endsection

@section('contenido')
@if(session('mensaje'))
<div class="alert alert-success">
    {{ session('mensaje') }}
</div>
@endif
<div class="row" id="container"> 
@foreach( $animales as $clave => $animal )
        
        <div class="card mb-3" id="cartaIndex">
        <div class="row g-0">
            <div class="col-md-4">
            

            <img class="img-fluid" id="imagenIndex" src="{{asset('/assets/img/' .$animal->imagen)}}"/>
            </div>
            <div class="col-md-8">
            <div class="card-body">
            
                <h3 class="card-title" style = "strong"><a href="{{ route('animales.show' , $animal ) }}">{{$animal->especie}}</a></h3>
                <lu>
                <li class="card-text">Peso: {{$animal->peso}} kg</li>
                <li class="card-text">Altura: {{$animal->altura}}</li>
                <li class="card-text">Edad: {{$animal->fechaNacimiento}}</li>
                <li class="card-text">Alimentación: {{$animal->alimentacion}}</li>
                <li class="card-text">Revisiones: {{count($animal->revisiones)}}</li>
                <li class="card-text">Cuidadores</li>
                <ol>
                @foreach($animal->cuidadores as $clave => $cuidador)
                <li class="card-text"><a href="{{ route('cuidadores.show' , $cuidador->id ) }}">{{$cuidador->nombre}}</a></li>
                @endforeach
                </ol>
                </ul>
               
                </div>
            </div>
        </div>
        </div>
    @endforeach
</div> 
@endsection