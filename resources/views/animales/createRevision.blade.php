@extends('layouts.master')

@section('titulo')
    zoologico
@endsection

@section('contenido')
@if(session('mensaje'))
<div class="alert alert-danger">
    {{session('mensaje')}}
</div>
@endif
<div class="row">     
    <div class="offset-md-3 col-md-6">        
        <div class="card">           
            <div class="card-header text-center">             
                 Crear una revisión para {{$animal->especie}}         
            </div>           
            <div class="card-body" style="padding:30px"> 
 
            <form method="post" action= "{{ route('animales.addRevision') }}" enctype="multipart/form-data">             
                    @csrf  
                    <input type="hidden" name='id_animal' value="{{$animal->id}}">
                    <div class="form-group">
                        <label for="fechaNacimiento">Fecha</label>                  
                        <input type="date" name="fechaRevision" id="fechaRevision" class="form-control" required>              
                    </div> 

                    <div class="form-group">                                         
                        <label for="descripcion">Descripción</label>                 
                        <textarea name="descripcion" id="descripcion" class="form-control" rows="3"></textarea>             
                    </div> 

                    <div class="form-group text-center">                
                        <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">             
                        Añadir Revision               
                    </button>              
                    </div>              
                </form>                      
            </div>       
        </div>     
    </div>  
</div>
@endsection