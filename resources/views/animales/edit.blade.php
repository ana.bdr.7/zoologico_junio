@extends('layouts.master')

@section('titulo')
    zoologico
@endsection

@section('contenido')
@if(session('mensaje'))
<div class="alert alert-success">
    {{session('mensaje')}}
</div>
@endif
<div class="row">     
    <div class="offset-md-3 col-md-6">        
        <div class="card">           
            <div class="card-header text-center">             
                 Editar animal           
            </div>           
            <div class="card-body" style="padding:30px"> 
 
            <form method="post" action="{{ route('animales.update',$animal->id) }}" enctype="multipart/form-data">             
                    @csrf
                    @method('put')
                    <input type="hidden" value="{{$animal->id}}" name="id" id="id">            
                    <div class="form-group">                 
                        <label for="especie">Especie</label>                 
                        <input type="text" name="especie" id="especie" class="form-control" value="{{$animal->especie}}" required>              
                    </div> 

                    <div class="form-group">
                        <label for="peso">Peso</label>                 
                        <input type="number" name="peso" id="peso" class="form-control" value="{{$animal->peso}}" required>             
                    </div> 

                    <div class="form-group">
                        <label for="altura">Altura</label>                  
                        <input type="number" name="altura" id="altura" class="form-control" value="{{$animal->altura}}" required>              
                    </div> 

                    <div class="form-group">
                        <label for="fechaNacimiento">Fecha de Nacimiento</label>                  
                        <input type="date" name="fechaNacimiento" id="fechaNacimiento" class="form-control" value="{{$animal->fechaNacimiento}}" required>              
                    </div> 

                    <div class="form-group">  
                        <label for="alimentacion">Alimentacion</label>               
                        <input type="text" name="alimentacion" id="alimentacion" class="form-control" value="{{$animal->alimentacion}}" required>                   
                    </div> 

                    <div class="form-group">                                         
                        <label for="descripcion">Descripción</label>                 
                        <textarea name="descripcion" id="descripcion" class="form-control" rows="3">{{$animal->descripcion}}</textarea>             
                    </div> 

                    <div class="form-group">    
                        <label for="imagen">Imagen</label>                                     
                        <input type="file" name="imagen" id="imagen" class="form-control" required>             
                    </div> 

                    <div class="form-group text-center">                
                        <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">             
                        Modificar                
                    </button>              
                    </div> 
                               
                </form>
            </div>       
        </div>     
    </div>  
</div>
@endsection