@extends('layouts.master')

@section('titulo')
    zoologico
@endsection

@section('contenido')
@if(session('mensaje'))
<div class="alert alert-success">
    {{ session('mensaje') }}
</div>
@endif
<div class="row" id="container"> 
    <div class="col-sm-3">     
        <img class="imagen" src="{{asset('/assets/img/' .$animal->imagen)}}">
    </div> 
    <div class="col-sm-9"> 
    
       <p><strong>{{$animal->especie}} {{$animal->getEdad()}} años</strong></p>
       <p><strong>Peso: </strong>{{$animal->peso}}</p>
       <p><strong>Altura: </strong>{{$animal->altura}}</p>
       <p><strong>Fecha de Nacimiento: </strong>{{$animal->fechaNacimiento}}</p>       
       <p><strong>Descripcion: </strong>{{$animal->descripcion}}</p>
       <p><strong>Revisiones: </strong></p>
       <ul>
       @foreach($animal->revisiones as $revision)       
       <li>{{$revision->fechaRevision}} : {{$revision->descripcion}}</li>
       @endforeach
        </ul>
      
   
    </ul>
       <a id ="botones" class="btn btn-success" href="/animales/{{$animal->id}}/editar" role="button">Editar</a>
       <a id ="botones" class="btn btn-success" href="/animales/{{$animal->id}}/createRevision" role="button">Añadir Revisión</a>
       <a id="botones" class="btn btn-success" href="/animales" role="button">Volver al listado</a>
    </div> 
</div> 
 
@endsection