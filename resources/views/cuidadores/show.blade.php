@extends('layouts.master')

@section('titulo')
    zoologico
@endsection

@section('contenido')
@if(session('mensaje'))
<div class="alert alert-success">
    {{ session('mensaje') }}
</div>
@endif
<div class="row" id="container"> 
    
    <div class="col-sm-9"> 
    <h2>{{$cuidador->nombre}}</h2>
    <h5>Titulaciones</h5>
    <ul>
    @foreach($cuidador->titulaciones() as $titulacion)
    <li><a href="{{ route('titulaciones.show' , $titulacion->id ) }}">{{$titulacion->nombre}}</a></li>
    @endforeach
    </ul>
      
    </div> 
</div> 
 
@endsection