@extends('layouts.master')

@section('titulo')
    zoologico
@endsection

@section('contenido')
@if(session('mensaje'))s
<div class="alert alert-success">
    {{ session('mensaje') }}
</div>
@endif
<div class="row" id="container"> 
    
    <div class="col-sm-9"> 
    <h2>{{$titulacion->nombre}}</h2>
    <h5>Titulados</h5>
    <ul>
    @foreach($titulacion->cuidadores as $cuidador)
    <li><a href="{{ route('cuidadores.show' , $cuidador->id ) }}">{{$cuidador->nombre}}</a></li>
    @endforeach
    </ul>
      
    </div> 
</div> 
 
@endsection