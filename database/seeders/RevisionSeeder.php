<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Revision;

class RevisionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

     private $revisiones = array(
         array(
             'id_animal' => 1,
             'descripcion' => 'primera revisión todo bien',
             'fechaRevision' => '2020-06-01',
         ),array(
            'id_animal' => 1,
            'descripcion' => 'segunda revisión todo bien',
            'fechaRevision' => '2020-09-11',
         )
         );
    public function run()
    {
        foreach($this->revisiones as $revision){
            $a = new Revision();
            $a->id_animal = $revision['id_animal'];
            $a->descripcion = $revision['descripcion'];
            $a->fechaRevision = $revision['fechaRevision'];            
            $a->save();
        }
        $this->command->info('Tabla Revisiones inicializada con datos');
    }
}
