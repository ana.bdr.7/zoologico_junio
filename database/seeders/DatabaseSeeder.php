<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Database\Seeders\AnimalSeeder;
use Database\Seeders\CuidadorSeeder;
use App\Models\Titulacion;
use App\Models\User;
use App\Models\Cuidador;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('animal_cuidador')->truncate();
        DB::table('revisions')->truncate();
        DB::table('cuidadors')->truncate();
        DB::table('animals')->truncate();
        DB::table('users')->truncate();
        DB::table('titulacions')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Titulacion::factory(10)->create();
        Cuidador::factory(20)->create();
        DB::table('animals')->delete();
        $this->call(AnimalSeeder::class);       
        User::factory(5)->create();
        DB::table('revisions')->delete();
        $this->call(RevisionSeeder::class);
    }
}
